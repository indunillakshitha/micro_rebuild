<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courts', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('court_category_id')->unsigned()->index();  
            $table->foreign('court_category_id')->references('id')->on('court_categories');

            $table->string('name',100);
            $table->integer('mobile')->nullable();
            $table->string('email',20)->nullable();
            $table->string('description',200)->nullable();

            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courts');
    }
}
