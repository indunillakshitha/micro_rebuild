<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_array =[


            //Users Section
            ['section_name'=>'users','name' => 'users.index'],
            ['name' => 'users.create','section_name'=>'users'],
            ['name' => 'users.store','section_name'=>'users'],
            ['name' => 'users.block','section_name'=>'users'],
            ['name' => 'users.update','section_name'=>'users'],
            ['name' => 'users.edit','section_name'=>'users'],

            //Roles Sectio
            ['name' => 'roles.index','section_name'=>'role'],
            ['name' => 'roles.create','section_name'=>'role'],
            ['name' => 'roles.store','section_name'=>'role'],
            ['name' => 'roles.delete','section_name'=>'role'],
            ['name' => 'roles.update','section_name'=>'role'],
            ['name' => 'roles.edit','section_name'=>'role'],


        ];



        //************ UNCOMMENT THESE SECTION ON FIRST SEED************//
        $user ['name']='Super admin';
        $user ['email']='suadmin@gmail.com';
        $user ['password']=Hash::make('Abcd@1234');

        $role = Role::create(['name' => 'Super Admin']);
        $role=Role::where('name','Super Admin')->first();
        foreach($permission_array as $permission){

            $check_has_permission = Permission::where('name', $permission['name'])->first();
            if(!isset($check_has_permission)){
                Permission::create($permission);
            }

            $role->givePermissionTo($permission['name']);
        }

        $user = User::create($user);
        $user =User::where('email','suadmin@gmail.com')->first();
        $user->assignRole('Super Admin');
    }
}
